package com.pachouri.globaldesk.activity;

/**
 * Created by ankit on 6/5/17.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Activity to keep common methods, variables etc. can be used by child activities in common
 * to avoid code redundancy
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Method to check internet connection available or not
     *
     * @param context
     * @return
     */
    public boolean isInternetConnected(Context context) {
        boolean isInternetConnected = false;
        try {
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context
                    .CONNECTIVITY_SERVICE);
            if (connManager.getActiveNetworkInfo() != null && connManager.getActiveNetworkInfo()
                    .isAvailable() && connManager.getActiveNetworkInfo().isConnected()) {
                isInternetConnected = true;
            }
        } catch (Exception ex) {
            isInternetConnected = false;
        }
        return isInternetConnected;
    }
}
