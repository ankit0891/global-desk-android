## Introduction: ##

*The application gives brief information for a Country selected. It has 200+ countries list and gives the details like currency, capital, region, subregion, population etc. for most of the countries.
*
## Name: Global Desk

## Why Global Desk? ##

As the app is the handbook for the Countries in the World. The Globe helps us with the same. So this app is all about the same.

## Assumptions: ##

* As pagination is not availabe in the api given. Have not implemented load more. And fetching the complete list of all countries in one call.
* Every information is not necessary to be shown. So have used the filter api call to get only specified fields in response.
* Have tried to keep the UI and UX as simple as possible so it would be easier to explore the app.
* There is a feature to change the order of the list. So have kept the design as initially "A-Z" order. The list is in increasing order(A-Z). If Tapped on icon, design changes to "Z-A" and list order changes to decreasing order (Z-A). I have assumed that whatever the order is going on it should be displayed to the user notifying that this is the current order. Instead I could have done in the other way (Showing the opposite design "Z-A" for the opposite order A-Z). That would mean tapping on the icon would toggle the list. Which is also correct. But as I have used a down arrow icon. So it would support the assumption. May be I am wrong. :)
* As the app is related with Country and its details. Have used the launcher icon and splash screen as Globe with flags.
* Some flag images are not availabe. So have used the splash screen icon as the placeholder.
* Some details are not available. So have mentioned "Detail not available" with respect to the field.

## Known Bugs: ##

*As I could not give too much time on the assignment. But still have tried to achieve as much as I could.* 
* If you apply Alphabetical filter first and then search, it won't search in the specific filtered list. It would search throught the list and vice versa.
* If you attempt the step 1 which is a bug. It raises to another bug. The alphabetical filter should get unchecked. But it remains applied.

## References: ##

* Collected images and icons from google.co.in, iconfinder.com etc.