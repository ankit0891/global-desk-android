package com.pachouri.globaldesk.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pachouri.globaldesk.R;

/**
 * Splash Activity that launches initially
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashTimer();
    }

    /**
     * Method to halt screen for sometime(here 1.5 sec) and
     * later open HomeActivity
     */
    private void splashTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent openHomeActivity = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(openHomeActivity);
                finish();
            }
        }, 1500);
    }
}
