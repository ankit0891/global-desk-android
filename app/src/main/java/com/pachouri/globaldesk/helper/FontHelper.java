package com.pachouri.globaldesk.helper;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by ankit on 6/5/17.
 */

/**
 * Class to apply font
 */
public class FontHelper {

    public static final int FONT_MONTESERRAT = 1;

    private static final int LIGHT = 0;
    private static final int REGULAR = 1;
    private static final int MEDIUM = 2;
    private static final int BOLD = 3;
    private static final int SEMI_BOLD = 4;

    private static final String MONTESERRAT_PATH = "fonts/Monteserrat/";

    private static final String MONTESERRAT_LIGHT = MONTESERRAT_PATH + "Montserrat-Light.ttf";
    private static final String MONTESERRAT_REGULAR = MONTESERRAT_PATH + "Montserrat-Regular.ttf";
    private static final String MONTESERRAT_MEDIUM = MONTESERRAT_PATH + "Montserrat-Medium.ttf";
    private static final String MONTESERRAT_BOLD = MONTESERRAT_PATH + "Montserrat-Bold.ttf";
    private static final String MONTESERRAT_SEMIBOLD = MONTESERRAT_PATH + "Montserrat-SemiBold.ttf";

    private String currentFontLight;
    private String currentFontRegular;
    private String currentFontMedium;
    private String currentFontBold;
    private String currentFontSemiBold;

    public FontHelper(int currentFont) {
        switch (currentFont) {
            case FONT_MONTESERRAT:
                currentFontLight = MONTESERRAT_LIGHT;
                currentFontRegular = MONTESERRAT_REGULAR;
                currentFontMedium = MONTESERRAT_MEDIUM;
                currentFontBold = MONTESERRAT_BOLD;
                currentFontSemiBold = MONTESERRAT_SEMIBOLD;
                break;
        }
    }

    public Typeface getFont(Context context, int fontIndex) {
        Typeface typeface;
        switch (fontIndex) {
            case LIGHT:
                typeface = FontCache.getFont(context, currentFontLight);
                break;
            case REGULAR:
                typeface = FontCache.getFont(context, currentFontRegular);
                break;
            case MEDIUM:
                typeface = FontCache.getFont(context, currentFontMedium);
                break;
            case BOLD:
                typeface = FontCache.getFont(context, currentFontBold);
                break;
            case SEMI_BOLD:
                typeface = FontCache.getFont(context, currentFontSemiBold);
                break;
            default:
                typeface = FontCache.getFont(context, currentFontRegular);
                break;
        }
        return typeface;
    }

}
