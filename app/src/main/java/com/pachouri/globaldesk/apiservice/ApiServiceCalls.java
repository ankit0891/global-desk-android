package com.pachouri.globaldesk.apiservice;

/**
 * Created by ankit on 6/5/17.
 */

import com.pachouri.globaldesk.model.CountryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface to implement the ApiCalls
 */
public interface ApiServiceCalls {

    //String to add in the base url to complete the url
    String countriesAll = "all";

    //Get call to get the list of Countries with only limited fields
    @GET(countriesAll)
    Call<List<CountryModel>> getCountryListApi(@Query(WebServiceCall.FIELDS_CALL) String fieldsCall);
}
