package com.pachouri.globaldesk.helper;

/**
 * Created by ankit on 6/5/17.
 */

/**
 * Class to main the constants used in the app at one place
 */
public class Constants {
    //constant to use font through out the app
    public static final int APP_FONTS = FontHelper.FONT_MONTESERRAT;


    /**
     * Intent keys - need to be used as constant, because using hard coded intent key is vulnerable
     */
    public static String KEY_COUNTRY_OBJECT = "key_country_object";
}
