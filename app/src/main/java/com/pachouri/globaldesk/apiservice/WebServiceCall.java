package com.pachouri.globaldesk.apiservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pachouri.globaldesk.model.CountryModel;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ankit on 6/5/17.
 */

/**
 * Class to initialise and setup Retrofit.
 * Webservice calls are declared
 */
public class WebServiceCall {

    final public static String FIELDS_CALL = "fields";
    public static ApiServiceCalls apiServiceCalls;
    private Gson gson;

    public static WebServiceCall webServiceCall;

    public static WebServiceCall getWebServiceCall() {
        if (webServiceCall == null) {
            webServiceCall = new WebServiceCall();
        }
        return webServiceCall;
    }

    //another interface to implement the callback success and failure
    public interface ApiCallBackHandler<T> {
        void onSuccess(T response);

        void onError(String message);
    }

    //Setup retrofit
    public ApiServiceCalls getApi() {
        if (apiServiceCalls == null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier
                    .STATIC);
            gsonBuilder.excludeFieldsWithoutExposeAnnotation();
            gson = gsonBuilder.create();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(loggingInterceptor);
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                }
            });
            Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
            String url = "https://restcountries.eu/rest/v2/";
            retrofitBuilder.baseUrl(url);
            retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson));
            retrofitBuilder.client(httpClient.build());
            Retrofit retrofit = retrofitBuilder.build();
            apiServiceCalls = retrofit.create(ApiServiceCalls.class);
        }
        return apiServiceCalls;
    }

    //Country List Api Call implementation with certain required fields
    public void getCountryList(final ApiCallBackHandler<List<CountryModel>> handler) {
        Call<List<CountryModel>> getCountryListCall = getApi().getCountryListApi("name;callingCodes;capital;region;subregion;population;latlng;currencies;languages;alpha2Code");
        getCountryListCall.enqueue(new ApiCallBack<List<CountryModel>>() {

            @Override
            public void onSuccess(List<CountryModel> response) {
                handler.onSuccess(response);
            }

            @Override
            public void onFailure(String message) {
                handler.onError(message);
            }
        });
    }
}
