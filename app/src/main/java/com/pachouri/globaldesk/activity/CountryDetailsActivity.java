package com.pachouri.globaldesk.activity;

/**
 * Created by ankit on 6/5/17.
 */

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.pachouri.globaldesk.R;
import com.pachouri.globaldesk.helper.Constants;
import com.pachouri.globaldesk.model.CountryModel;
import com.pachouri.globaldesk.widget.AppTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity opens up on Tapping Country, showing the details of the respective country
 */
public class CountryDetailsActivity extends BaseActivity {

    @BindView(R.id.collapsingToolbar)
    protected CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.imageViewCountryFlag)
    protected ImageView imageViewCountryFlag;

    @BindView(R.id.textViewCapital)
    protected AppTextView textViewCapital;

    @BindView(R.id.textViewRegion)
    protected AppTextView textViewRegion;

    @BindView(R.id.textViewSubRegion)
    protected AppTextView textViewSubRegion;

    @BindView(R.id.textViewCallingCode)
    protected AppTextView textViewCallingCode;

    @BindView(R.id.textViewCurrency)
    protected AppTextView textViewCurrency;

    @BindView(R.id.textViewLanguages)
    protected AppTextView textViewLanguages;

    @BindView(R.id.textViewPopulation)
    protected AppTextView textViewPopulation;
    @BindView(R.id.textViewLocation)
    protected AppTextView textViewLocation;

    private CountryModel country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        getIntentData();
        setTranslucentStatusBar();
        setToolbar();
        setValues();
    }

    /**
     * To get the intent data
     */
    private void getIntentData() {
        Gson gson = new Gson();
        country = gson.fromJson(getIntent().getStringExtra(Constants.KEY_COUNTRY_OBJECT), CountryModel.class);
    }

    /**
     * To set the status bar as translucent (To show the image to the top)
     */
    private void setTranslucentStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * To set toolbar with back arrow and text on collapsing and expansion of CollapsingToolbar
     */
    private void setToolbar() {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setTitle(country.getName());
            toolbar.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));

            final Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Monteserrat/Montserrat-SemiBold.ttf");
            collapsingToolbar.setCollapsedTitleTypeface(typeface);
            collapsingToolbar.setExpandedTitleTypeface(typeface);
        }
    }

    /**
     * To get the drawable (int) by using the string
     *
     * @param imageName
     * @return
     */
    public int getImage(String imageName) {
        int drawableResourceId = getResources().getIdentifier(imageName, "drawable", getPackageName());
        return drawableResourceId;
    }

    /**
     * To set the values of Country Details in the respective controls
     */
    private void setValues() {
        int flagImage = getImage(country.getAlpha2Code().toLowerCase());
        if (flagImage != 0) {
            Glide.with(getApplicationContext()).load(flagImage).into(imageViewCountryFlag);
        } else {
            Glide.with(getApplicationContext()).load(R.drawable.ic_splash).into(imageViewCountryFlag);
        }

        checkEmptyAndSetValue(textViewCapital, country.getCapital());
        checkEmptyAndSetValue(textViewRegion, country.getRegion());
        checkEmptyAndSetValue(textViewSubRegion, country.getSubregion());

        if (!country.getCallingCodes().isEmpty()) {
            String callingCode = "";
            if (country.getCallingCodes().size() > 1) {
                callingCode = TextUtils.join(", ", country.getCallingCodes());
            } else {
                callingCode = country.getCallingCodes().get(0).toString();
            }
            textViewCallingCode.setText(callingCode);
        } else {
            textViewCallingCode.setText(getString(R.string.detail_not_available));
        }

        if (!country.getCurrencies().isEmpty()) {
            String currency = "";
            if (country.getCurrencies().size() > 1) {
                List<String> currencyList = new ArrayList<>();
                for (int loop = 0; loop < country.getCurrencies().size(); loop++) {
                    currencyList.add(country.getCurrencies().get(loop).getName());
                }
                currency = TextUtils.join(", ", currencyList);
            } else {
                currency = country.getCurrencies().get(0).getName().toString();
            }
            textViewCurrency.setText(currency);
        } else {
            textViewCurrency.setText(getString(R.string.detail_not_available));
        }

        if (!country.getLanguages().isEmpty()) {
            String language = "";
            if (country.getLanguages().size() > 1) {
                List<String> languagesList = new ArrayList<>();
                for (int loop = 0; loop < country.getLanguages().size(); loop++) {
                    languagesList.add(country.getLanguages().get(loop).getName());
                }
                language = TextUtils.join(", ", languagesList);
            } else {
                language = country.getLanguages().get(0).getName().toString();
            }
            textViewLanguages.setText(language);
        } else {
            textViewLanguages.setText(getString(R.string.detail_not_available));
        }

        checkEmptyAndSetValue(textViewPopulation, country.getPopulation() + "");

        if (!country.getLatlng().isEmpty()) {
            String location = TextUtils.join(", ", country.getLatlng());
            textViewLocation.setText(location);
        } else {
            textViewLocation.setText(getString(R.string.detail_not_available));
        }
    }

    /**
     * Method to check set values, if empty set value as "Detail not availabe"
     *
     * @param appTextView
     * @param textValue
     */
    private void checkEmptyAndSetValue(AppTextView appTextView, String textValue) {
        if (!TextUtils.isEmpty(textValue)) {
            appTextView.setText(textValue);
        } else {
            appTextView.setText(getString(R.string.detail_not_available));
        }
    }

    /**
     * OnClick event on Pin Marker image to open the coordinates through maps application
     */
    @OnClick(R.id.imageViewMarker)
    protected void onMarkerClicked() {
        String uri = "http://maps.google.com/maps?q=loc:" + country.getLatlng().get(0) + "," + country.getLatlng().get(1) + " (" + country.getName() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
