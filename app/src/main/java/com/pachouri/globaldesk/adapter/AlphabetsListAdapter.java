package com.pachouri.globaldesk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pachouri.globaldesk.R;
import com.pachouri.globaldesk.widget.AppTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ankit on 6/5/17.
 */
public class AlphabetsListAdapter extends RecyclerView.Adapter<AlphabetsListAdapter.AlphabetViewHolder> {

    private Context context;
    private List<String> alphabetList;
    private AlphabetListAdapterListener alphabetListAdapterListener;
    private int selectedPosition = 0;

    public AlphabetsListAdapter(Context context, List<String> alphabetList) {
        this.context = context;
        this.alphabetList = alphabetList;
    }

    @Override
    public AlphabetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_alphabets, parent, false);
        AlphabetViewHolder countryListViewHolder = new AlphabetViewHolder(view);
        return countryListViewHolder;
    }


    @Override
    public void onBindViewHolder(AlphabetViewHolder holder, final int position) {
        if (!alphabetList.isEmpty()) {
            holder.textViewAlphabet.setText(alphabetList.get(position).toString() + "");
            if (selectedPosition == position) {
                holder.itemView.setSelected(true);
            } else {
                holder.itemView.setSelected(false);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition = position;
                    notifyDataSetChanged();
                    alphabetListAdapterListener.onAlphabetClicked(alphabetList.get(position).toString());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return alphabetList.size();
    }

    public class AlphabetViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewAlphabet)
        protected AppTextView textViewAlphabet;

        public AlphabetViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Method to set listener from Activity
     *
     * @param alphabetListAdapterListener
     */
    public void setAlphabetItemListener(AlphabetListAdapterListener alphabetListAdapterListener) {
        this.alphabetListAdapterListener = alphabetListAdapterListener;
    }

    /**
     * On Item click listener interface for Alphabet list
     */
    public interface AlphabetListAdapterListener {
        void onAlphabetClicked(String alphabet);
    }
}
