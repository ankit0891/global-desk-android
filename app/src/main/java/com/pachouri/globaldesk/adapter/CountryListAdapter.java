package com.pachouri.globaldesk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.pachouri.globaldesk.R;
import com.pachouri.globaldesk.model.CountryModel;
import com.pachouri.globaldesk.widget.AppTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ankit on 6/5/17.
 */
public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.CountryListViewHolder> {

    private Context context;
    private List<CountryModel> countryModelList;
    private CountryListAdapterListener countryListAdapterListener;
    private int lastPosition = -1;
    private ArrayList<CountryModel> arrayList;

    public CountryListAdapter(Context context, List<CountryModel> countryModelList) {
        this.context = context;
        this.countryModelList = countryModelList;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(countryModelList);
    }

    @Override
    public CountryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_country, parent, false);
        CountryListViewHolder countryListViewHolder = new CountryListViewHolder(view);
        return countryListViewHolder;
    }


    @Override
    public void onBindViewHolder(CountryListViewHolder holder, int position) {
        if (!countryModelList.isEmpty()) {
            CountryModel countryObject = countryModelList.get(position);
            int flagImage = getImage(countryObject.getAlpha2Code().toLowerCase());
            if (flagImage != 0) {
                Glide.with(context).load(flagImage).into(holder.imageViewCountryFlag);
            } else {
                Glide.with(context).load(R.drawable.ic_splash).into(holder.imageViewCountryFlag);
            }
            holder.textViewCountryName.setText(countryObject.getName());
            holder.textViewRegion.setText("Region in " + countryObject.getRegion());
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.anim_scroll_up
                            : R.anim.anim_scroll_down);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
    }

    /**
     * Method to get the int drawable from the string name passed
     *
     * @param imageName
     * @return
     */
    public int getImage(String imageName) {
        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return drawableResourceId;
    }

    @Override
    public void onViewDetachedFromWindow(CountryListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return countryModelList.size();
    }

    public class CountryListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageViewCountryFlag)
        protected ImageView imageViewCountryFlag;

        @BindView(R.id.textViewCountryName)
        protected AppTextView textViewCountryName;

        @BindView(R.id.textViewRegion)
        protected AppTextView textViewRegion;

        public CountryListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            countryListAdapterListener.onCountryItemClicked(countryModelList.get(getLayoutPosition()));
        }
    }

    /**
     * Method to set the listener
     *
     * @param countryListAdapterListener
     */
    public void setCountryItemListener(CountryListAdapterListener countryListAdapterListener) {
        this.countryListAdapterListener = countryListAdapterListener;
    }

    /**
     * On Item Click Listener interface for Country list item
     */
    public interface CountryListAdapterListener {
        void onCountryItemClicked(CountryModel country);
    }

    /**
     * Method to filter the list on search
     *
     * @param charText
     */
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        countryModelList.clear();
        if (charText.length() == 0) {
            countryModelList.addAll(arrayList);
        } else {
            for (CountryModel country : arrayList) {
                if (country.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    countryModelList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }

    /**
     * Method to filter the list alphabetically
     *
     * @param charText
     */
    public void filterAlphabet(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        countryModelList.clear();
        if (charText.length() == 0) {
            countryModelList.addAll(arrayList);
        } else {
            for (CountryModel country : arrayList) {
                if (country.getName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                    countryModelList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }
}
