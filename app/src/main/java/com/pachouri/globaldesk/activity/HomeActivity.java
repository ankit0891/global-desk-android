package com.pachouri.globaldesk.activity;

/**
 * Created by ankit on 6/5/17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pachouri.globaldesk.R;
import com.pachouri.globaldesk.adapter.AlphabetsListAdapter;
import com.pachouri.globaldesk.adapter.CountryListAdapter;
import com.pachouri.globaldesk.apiservice.WebServiceCall;
import com.pachouri.globaldesk.helper.Constants;
import com.pachouri.globaldesk.model.CountryModel;
import com.pachouri.globaldesk.widget.AppEditText;
import com.pachouri.globaldesk.widget.AppTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * HomeActivity (Landing screen) that opens up after SplashActivity
 * to display the list of Countries list
 */
public class HomeActivity extends BaseActivity implements CountryListAdapter.CountryListAdapterListener, AlphabetsListAdapter.AlphabetListAdapterListener {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.recyclerViewCountryList)
    protected RecyclerView recyclerViewCountryList;

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @BindView(R.id.editTextSearch)
    protected AppEditText editTextSearch;

    @BindView(R.id.textViewOrder)
    protected TextView textViewOrder;

    @BindView(R.id.recyclerViewAlphabetList)
    protected RecyclerView recyclerViewAlphabetList;

    @BindView(R.id.layoutMainContainer)
    protected LinearLayout layoutMainContainer;

    @BindView(R.id.layoutErrorContainer)
    protected LinearLayout layoutErrorContainer;

    @BindView(R.id.textViewErrorTitle)
    protected AppTextView textViewErrorTitle;

    @BindView(R.id.textViewErrorSubTitle)
    protected AppTextView textViewErrorSubTitle;

    private CountryListAdapter countryListAdapter;
    private boolean increaseOrder = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setToolbar();
        pullCountryList();
        prepareAlphabetsList();
    }

    /**
     * Method to set the toolbar
     */
    private void setToolbar() {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setTitle(getString(R.string.app_name));
        }
    }

    /**
     * Method to initialise and set the adapter
     */
    private void setCountryListAdapter(final List<CountryModel> countryList) {
        if (!countryList.isEmpty()) {
            countryListAdapter = new CountryListAdapter(getApplicationContext(), countryList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager
                    .VERTICAL, false);
            countryListAdapter.setCountryItemListener(this);
            recyclerViewCountryList.setLayoutManager(linearLayoutManager);
            recyclerViewCountryList.setAdapter(countryListAdapter);

            editTextSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String text = editTextSearch.getText().toString().toLowerCase(Locale.getDefault());
                    countryListAdapter.filter(text);
                    toggleOrderTextView(true);
                }
            });

            editTextSearch.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int DRAWABLE_RIGHT = 2;
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (editTextSearch.getRight() - editTextSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            editTextSearch.setText("");
                            editTextSearch.setHint("Search");
                            return true;
                        }
                    }
                    return false;
                }
            });

            textViewOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (increaseOrder) {
                        toggleOrderTextView(false);
                    } else {
                        toggleOrderTextView(true);
                    }
                    Collections.reverse(countryList);
                    countryListAdapter.notifyDataSetChanged();
                }
            });
        }
    }


    /**
     * Method to pull the country list and handle on success and failure response
     */
    private void pullCountryList() {
        layoutMainContainer.setVisibility(View.GONE);
        layoutErrorContainer.setVisibility(View.GONE);
        if (isInternetConnected(getApplicationContext())) {
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
            WebServiceCall webServiceCall = WebServiceCall.getWebServiceCall();
            if (null != webServiceCall) {
                webServiceCall.getCountryList(new WebServiceCall.ApiCallBackHandler<List<CountryModel>>() {
                    @Override
                    public void onSuccess(List<CountryModel> response) {
                        Log.v("onSuccess", " : " + response);
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                            layoutMainContainer.setVisibility(View.VISIBLE);
                            layoutErrorContainer.setVisibility(View.GONE);
                        }
                        setCountryListAdapter(response);
                    }

                    @Override
                    public void onError(String message) {
                        Log.v("onError", " : " + message);
                        showSomethingWrongLayout();
                    }
                });
            }
        } else {
            showInternetLayout();
        }
    }

    @Override
    public void onCountryItemClicked(CountryModel country) {
        if (null != country) {
            Gson gson = new Gson();
            String myJson = gson.toJson(country);
            Intent openDetails = new Intent(HomeActivity.this, CountryDetailsActivity.class);
            openDetails.putExtra(Constants.KEY_COUNTRY_OBJECT, myJson);
            startActivity(openDetails);
        }
    }

    /**
     * Method to show no internet available layout
     */
    private void showInternetLayout() {
        if (!isFinishing() && null != progressBar) {
            progressBar.setVisibility(View.GONE);
            layoutMainContainer.setVisibility(View.GONE);
            layoutErrorContainer.setVisibility(View.VISIBLE);
            textViewErrorTitle.setText(getString(R.string.error_oops));
            textViewErrorSubTitle.setText(getString(R.string.internet_not_found));
        }
    }

    /**
     * Method to show something went wrong layout
     */
    private void showSomethingWrongLayout() {
        if (!isFinishing() && null != progressBar) {
            progressBar.setVisibility(View.GONE);
            layoutMainContainer.setVisibility(View.GONE);
            layoutErrorContainer.setVisibility(View.VISIBLE);
            textViewErrorTitle.setText(getString(R.string.error_sorry));
            textViewErrorSubTitle.setText(getString(R.string.something_wrong));
        }
    }

    /**
     * OnClick event for TryAgain button
     */
    @OnClick(R.id.textViewTryAgain)
    protected void onTryAgainClicked() {
        pullCountryList();
    }

    /**
     * Method to generate alphabet list from A to Z and set the adapter
     */
    private void prepareAlphabetsList() {
        List<String> alphabetsList = new ArrayList<>();
        alphabetsList.add("ALL");
        for (char alphabet = 'A'; alphabet <= 'Z'; alphabet++) {
            alphabetsList.add(alphabet + "");
        }
        if (!alphabetsList.isEmpty()) {
            final AlphabetsListAdapter alphabetsListAdapter = new AlphabetsListAdapter(getApplicationContext(), alphabetsList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager
                    .HORIZONTAL, false);
            alphabetsListAdapter.setAlphabetItemListener(this);
            recyclerViewAlphabetList.setLayoutManager(linearLayoutManager);
            recyclerViewAlphabetList.setAdapter(alphabetsListAdapter);
        }
    }

    @Override
    public void onAlphabetClicked(String alphabet) {
        if (countryListAdapter != null) {
            toggleOrderTextView(true);
            if (!TextUtils.isEmpty(alphabet)) {
                if (alphabet.equalsIgnoreCase("ALL")) {
                    countryListAdapter.filterAlphabet("");
                } else {
                    countryListAdapter.filterAlphabet(alphabet);
                }
            } else {
                countryListAdapter.filterAlphabet("");
            }
        }
    }

    /**
     * To toggle the design and value for ordering the list
     *
     * @param isReset
     */
    private void toggleOrderTextView(boolean isReset) {
        if (isReset) {
            textViewOrder.setText(getString(R.string.a_z));
            increaseOrder = true;
        } else {
            textViewOrder.setText(getString(R.string.z_a));
            increaseOrder = false;
        }

    }
}